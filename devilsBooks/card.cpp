#include "card.h"

Card::Card(SUIT suit, RANK rank)
{
    features_.first = suit;
    features_.second = rank;

    switch (features_.second) {
    case Card::RANK::SIX:
        name_ += "6-";
        break;
    case Card::RANK::SEVEN:
        name_ += "7-";
        break;
    case Card::RANK::EIGHT:
        name_ += "8-";
        break;
    case Card::RANK::NINE:
        name_ += "9-";
        break;
    case Card::RANK::TEN:
        name_ += "10-";
        break;
    case Card::RANK::JACK:
        name_ += "J-";
        break;
    case Card::RANK::QUEEN:
        name_ += "Q-";
        break;
    case Card::RANK::KING:
        name_ += "K-";
        break;
    case Card::RANK::ACE:
        name_ += "A-";
        break;
    }

    switch (features_.first) {
    case Card::SUIT::HEARTS:
        name_+="HR";
        break;
    case Card::SUIT::DIAMONDS:
        name_+="DI";
        break;
    case Card::SUIT::CLUBS:
        name_+="CL";
        break;
    case Card::SUIT::SPADES:
        name_+="SP";
        break;
    }
}

const std::string & Card::getName() const
{
    return name_;
}

const Card::Features & Card::getFeatures() const
{
    return features_;
}
