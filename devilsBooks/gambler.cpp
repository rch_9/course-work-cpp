#include "gambler.h"
#include <ncursesw/curses.h>


Gambler::Gambler(std::unique_ptr<Deck> & deck, const Card::SUIT &trump):
    movesStock_(2),
    trump_(trump)
    //current_(0)
{
    for(int i = 0; i < 6; ++i)
    {
        takeCard(deck->pullCard());
    }
}

void Gambler::takeCard(const pCard card)
{
    hand_.push_back(card);
}

int Gambler::getMovesStock() const
{
    return movesStock_;
}
