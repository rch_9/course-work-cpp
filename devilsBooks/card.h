#ifndef CARD
#define CARD

#include <string>
#include <utility>

struct Card
{
    friend class PlayerTest;

    enum class SUIT
    {
        HEARTS = 0,
        DIAMONDS,
        CLUBS,
        SPADES
    } ;

    enum class RANK
    {
        SIX = 0,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING,
        ACE
    };

private:
    Card(const Card &);
    Card operator=(const Card &);

    typedef std::pair<SUIT, RANK> Features;

    Features features_;
    std::string name_;

public:    
    Card(SUIT suit, RANK rank);
    const Features & getFeatures() const;
    const std::string & getName() const;
};

#endif // CARD

