#include "player-test.h"
#include "../devilsBooks/player.h"
#include "../devilsBooks/card.h"
#include <vector>
#include <memory>
#include <iostream>

bool PlayerTest::checkTakeCard()
{
    std::vector<std::shared_ptr<Card>> cards;
    cards.push_back(std::make_shared<Card>(static_cast<Card::SUIT>(2), static_cast<Card::RANK>(4)));
    cards.push_back(std::make_shared<Card>(static_cast<Card::SUIT>(0), static_cast<Card::RANK>(3)));

    Player p;
    p.takeCard(cards.back());
    cards.pop_back();
    p.takeCard(cards.back());
    cards.pop_back();

    return (p.cards_.size() == 2) && (p.cards_.back().get()->getName() == "10-CL");
}

bool PlayerTest::checkPullCard()
{
    Player p;
    p.takeCard(std::make_shared<Card>(static_cast<Card::SUIT>(0), static_cast<Card::RANK>(3)));
    p.takeCard(std::make_shared<Card>(static_cast<Card::SUIT>(2), static_cast<Card::RANK>(4)));

    return (p.pullCard().get()->getName() == "10-CL") && (p.cards_.size() == 1);
}
