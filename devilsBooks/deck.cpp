#include "deck.h"
#include <iostream>
#include <vector>
#include <algorithm>

Deck::Deck()
{
    fill();
    shuffle();
}

void Deck::fill()
{
    for(unsigned i = 0; i < AMOUNT_SUITS; ++i)
    {
        for(unsigned j = 0; j < AMOUNT_RANKS; ++j)
        {
            deque_.push_back(std::make_shared<Card>(static_cast<Card::SUIT>(i), static_cast<Card::RANK>(j)));
        }
    }
}

void Deck::shuffle()
{
    std::random_shuffle(deque_.begin(), deque_.end());
}

Deck::pCard Deck::getLowerCard() const
{
    return deque_.front();
}

Deck::pCard Deck::pullCard()
{
    pCard card = deque_.back();
    deque_.pop_back();

    return card;
}

unsigned Deck::size() const
{
    return deque_.size();
}
