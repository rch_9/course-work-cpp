#ifndef GAMBLER
#define GAMBLER

#include <vector>
#include <memory>
#include "card.h"
#include "deck.h"


class Gambler
{
    friend class Renderer;
protected:
    typedef std::shared_ptr<Card> pCard;
public:
    /*
    enum class Action
    {
        ATTACK,
        REFLECT
    };
    */
    //как тут отнаследовать конструктор?
    Gambler(std::unique_ptr<Deck> & deck, const Card::SUIT & trump);

    //копируем указатель    
    void takeCard(const pCard card);
    //pCard pullCard();
    //pCard pullCurrentCard();

    int getMovesStock() const;

    //virtual void update(const int & pressedKey, Action action) = 0;
    virtual void draw() const = 0;
    virtual pCard attack() = 0;
    virtual pCard reflect(const pCard &) = 0;
protected:        
    std::vector<pCard> hand_;
    int movesStock_;
    Card::SUIT trump_;
    //int current_;
};

#endif // GAMBLER

