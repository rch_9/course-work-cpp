#ifndef DECK
#define DECK

#include <deque>
#include <memory>
#include "card.h"

class Deck
{
    typedef std::shared_ptr<Card> pCard;
public:
    friend struct DeckTest;
    Deck();

    pCard getLowerCard() const;
    pCard pullCard();

    unsigned size() const;

private:    
    Deck(const Deck &);
    Deck operator=(const Deck &);

    const unsigned MAX_SIZE = 36;
    const unsigned AMOUNT_SUITS = 4;
    const unsigned AMOUNT_RANKS = 9;

    void fill();
    void shuffle();

    std::deque<pCard> deque_;
};


#endif // DECK

