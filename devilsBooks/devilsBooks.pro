TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#QMAKE_CXXFLAGS += -lncursesw
LIBS += -lncursesw

SOURCES += main.cpp \
    card.cpp \
    deck.cpp \
    gametable.cpp \
    player.cpp \
    renderer.cpp \
    gambler.cpp \
    bot.cpp

HEADERS += \
    card.h \
    deck.h \
    gametable.h \
    gambler.h \
    player.h \
    renderer.h \
    bot.h

