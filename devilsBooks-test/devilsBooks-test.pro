TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    test.cpp \
    ../devilsBooks/card.cpp \
    ../devilsBooks/dealer.cpp \
    ../devilsBooks/deck.cpp \
    ../devilsBooks/player.cpp \
    deck-test.cpp \
    ../devilsBooks/gametable.cpp \
    player-test.cpp \
    gametable-test.cpp

HEADERS += \
    ../devilsBooks/card.h \
    ../devilsBooks/dealer.h \
    ../devilsBooks/deck.h \
    ../devilsBooks/player.h \
    deck-test.h \
    ../devilsBooks/gametable.h \
    player-test.h \
    gametable-test.h

DISTFILES += \
    last_deck_order.txt

