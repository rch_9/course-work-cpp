#ifndef PLAYER
#define PLAYER

#include <memory>
#include <utility>
#include "gambler.h"
#include "deck.h"

struct Player : public Gambler
{    
    Player(std::unique_ptr<Deck> & deck, const Card::SUIT & trump): Gambler(deck, trump) {}

    //void update(const int & pressedKey, Action action);
    void alterCurrent(const int & pressedKey);
    void draw() const;
    pCard attack();
    pCard reflect(const pCard & card);
private:    
    //int scanKyy() const;
    pCard pullCurrentCard();
    //std::pair<int, bool> action;
    int current_;
};

#endif // PLAYER

