#include <ctime>
#include "gametable.h"
#include "renderer.h"

int main()
{
    srand(time(0));
    GameTable gameTable(2);
    Renderer renderer;

    while(gameTable.getIsPlaying())
    {
        renderer.render(gameTable);
        gameTable.update();
        renderer.clear();
    }

    return 0;
}
