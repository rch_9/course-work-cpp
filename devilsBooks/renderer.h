#ifndef RENDERER
#define RENDERER

#include <utility>
#include <string>
#include <vector>
#include <memory>
#include <ncursesw/ncurses.h>
#include "gametable.h"


struct Renderer
{       
    Renderer();
    ~Renderer();

    void render(GameTable & gametable) const;
    void clear() const;
};

#endif // RENDERER

