#ifndef BOT
#define BOT

#include "gambler.h"

struct Bot : public Gambler
{
    Bot(std::unique_ptr<Deck> & deck, const Card::SUIT & trump) : Gambler(deck, trump)
    {}

    //void update(const int & pressedKey, Action action);
    void draw() const {}
    pCard attack() {}
    pCard reflect(const pCard & card) {}
private:
};

#endif // BOT

