#include "gametable.h"
#include <iostream>
#include <ncursesw/ncurses.h>

GameTable::GameTable(const unsigned amountOpponents):
    isPlaying_(true),
    deck_(new Deck),
    trump_(deck_.get()->getLowerCard())
{
    //
    attacker = 0;
    reflecter = (attacker + 1) % amountOpponents;

    gamblers_.push_back(std::make_shared<Player>(deck_, trump_.get()->getFeatures().first));
    for(unsigned i = 0; i < amountOpponents; ++i)
    {
        gamblers_.push_back(std::make_shared<Bot>(deck_, trump_.get()->getFeatures().first));
    }
}

bool GameTable::getIsPlaying() const
{
    return isPlaying_;
}

void GameTable::update()
{
    /*
    int pressedKey = getch();

    mvprintw(20, 20, "%d", pressedKey);
    if(pressedKey == 27)
    {
        isPlaying_ = false;
        return;
    }
    */
    //gamblers_.at(attacker).get()->attack();

    //lyingCards_.push_back();

    gamblers_.at(0).get()->attack();

    //сюда update();
    //gamblers_.at(0).get()->update(pressedKey, Player::Action::ATTACK);
}

void GameTable::chooseAttacker()
{
    ++attacker;
    attacker %= gamblers_.size();
    reflecter = (attacker + 1) % gamblers_.size();
}

void GameTable::draw() const
{
    int WIN_MAX_Y, WIN_MAX_X;
    getmaxyx(stdscr, WIN_MAX_Y, WIN_MAX_X);

    mvprintw(WIN_MAX_Y - 2, WIN_MAX_X - trump_.get()->getName().size() - 8, "Trump: ");
    printw(trump_.get()->getName().c_str());
    mvprintw(WIN_MAX_Y - 1, WIN_MAX_X - 18, "Cards in deck: %d", deck_.get()->size());
}
