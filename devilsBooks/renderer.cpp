#include "renderer.h"
#include <algorithm>
#include <ncursesw/ncurses.h>

Renderer::Renderer()
{
    initscr();
    noecho();
    keypad(stdscr, TRUE);
}

Renderer::~Renderer()
{
    endwin();
}

void Renderer::render(GameTable & gametable) const
{
    unsigned WIN_SIZE_Y, WIN_SIZE_X;
    getmaxyx(stdscr, WIN_SIZE_Y, WIN_SIZE_X);       

    gametable.draw();
    gametable.gamblers_.front().get()->draw();
    refresh();
}

void Renderer::clear() const
{       
    ::clear();
}
