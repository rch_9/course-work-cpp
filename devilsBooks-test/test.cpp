#include "deck-test.h"
#include "player-test.h"
#include <ctime>
#include <stdio.h>

#define BOOST_TEST_MODULE MyTest
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_CASE(deck_test)
{
    srand(time(0));
    DeckTest test;
    //test.checkDeck();
}


BOOST_AUTO_TEST_CASE(player_test)
{
    PlayerTest test;

    BOOST_CHECK(test.checkTakeCard());
    BOOST_CHECK(test.checkPullCard());
}
