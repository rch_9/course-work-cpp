#ifndef GAMETABLE
#define GAMETABLE

#include <vector>
#include <memory>
#include <utility>
#include "deck.h"
#include "gambler.h"
#include "player.h"
#include "bot.h"

class GameTable
{
public:
    friend class Renderer;

    GameTable(const unsigned amountOpponents);    

    void update();
    void draw() const;
    bool getIsPlaying() const;

private:
    typedef std::shared_ptr<Card> pCard;
    void chooseAttacker();

    bool isPlaying_;
    int attacker, reflecter;
    std::unique_ptr<Deck> deck_;
    std::vector<std::shared_ptr<Gambler>> gamblers_;
    pCard trump_;
    std::vector<std::pair<pCard, pCard>> lyingCards_;
};

#endif // GAMETABLE

